# InfluxDB Gotify notification endpoint connector

# ‼️ This version is super alpha, no caution required though. It will be made into a docker image and after that rewritten in GO to be used as a Gotify plugin ‼️

## How to use

1. Edit `launch.sh` according to your setup. Make sure that this runs in the same network your InfluxDB is in.
2. Add HTTP endpoint in InfluxDB and set address the same as this container's `--name` (in the script).
3. Be Gotified or whatever.