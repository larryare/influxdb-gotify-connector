from flask import Flask, request, Response
import json
import gotify

app = Flask(__name__)

@app.route('/', methods=['POST'])
def result():
    request_dict = request.get_json()

    gotify_obj = gotify.gotify(
    base_url="http://gotify:80",
    app_token="AAD9VUx6xur8Zny",
    )

    gotify_obj.create_message(
    request_dict['_message'] +
    '\nNotification rule name: ' +
    request_dict['_notification_rule_name'] +
    '\nMeasurement ' +
    request_dict['_source_measurement'] +
    'is ' + 
    str(request_dict['iaq']),
    title=request_dict['_check_name'],
    priority=10,
    )

    status_code = Response(status=200)
    return status_code